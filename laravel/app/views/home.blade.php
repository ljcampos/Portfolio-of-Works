@extends('layouts.default')
@section('content')
<!-- Main -->
<div id='main'>
	<!-- One -->
	<section id='one'>
		<header>
			<h2> Ipsum lorem dolor aliquam ante commodo<br />
			magna sed accumsan arcu neque.</h2>
		</header>
		<p>Accumsan orci faucibus id eu lorem semper. Eu ac iaculis ac nunc nisi lorem vulputate lorem neque cubilia ac in adipiscing in curae lobortis tortor primis integer massa adipiscing id nisi accumsan pellentesque commodo blandit enim arcu non at amet id arcu magna. Accumsan orci faucibus id eu lorem semper nunc nisi lorem vulputate lorem neque cubilia.</p>
		<ul class="actions">
			<li><a href="#" class="button">Learn More</a></li>
		</ul>
	</section>

	<!-- Two -->
	<section id='two'>
		<h2>Recent Work</h2>
		<div class='row'>
			<article class='6u 12u$(xsmall) work-item'>
				<a href='image/fulls/01.jpg' class='image fit thumb'><img src='../image/thumbs/01.jpg'></a>
				<h3>Magna sed consequant tempus</h3>
				<p>Lorem ipsum dolor sit amet, consectetur</p>
			</article>

			<article class='6u$ 12u$(xsmall) work-item'>
				<a href='image/fulls/05.jpg' class='image fit thumb'><img src='../image/thumbs/06.jpg' alt='...' /></a>
				<h3>Risus ornare lacinia</h3>
				<p>Lorem ipsum dolor sit amet nisl sed nullam feugiat.</p>
			</article>

			<article class='6u 12u$(xsmall) work-item'>
				<a href='image/fulls/02.jpg' class='image fit thumb'><img src='../image/thumbs/02.jpg' alt='...' /></a>
				<h3>Ultricies lacinia interdum</h3>
				<p>Lorem ipsum dolor sit amet nisl sed nullam feugiat.</p>
			</article>

			<article class="6u$ 12u$(xsmall) work-item">
				<a href='image/fulls/04.jpg' class='image fit thumb'><img src='../image/thumbs/04.jpg' alt='' /></a>
				<h3>Quam neque phasellus</h3>
				<p>Lorem ipsum dolor sit amet nisl sed nullam feugiat.</p>
			</article>
		</div>
		<ul class="actions">
			<li><a href="#" class="button">Full Portfolio</a></li>
		</ul>
	</section>

	<section id='three'>
		<h2>Get In Touch</h2>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		quis nostrud exercitation ullamco.</p>
		<div class='row'>
			<div class='8u 12u$(small)'>
				<form method='post' action='#'>
					<div class='row uniform 50%'>
						<div class='6u 12u$(xsmall)'>
							<input type='text' name='name' id='name' placeholder='Name'/>
						</div>
						<div class='6u 12u$(xsmall)'>
							<input type='email' name='email' id='email' placeholder='Email'/>
						</div>
						<div class='12u$'>
							<textarea name='message' id='message' placeholder='Message' rows='4'></textarea>
						</div>
					</div>
				</form>
				<ul class='actions'>
					<li><input type='submit' value='Send Message' /></li>
				</ul>
			</div>
			<div class='4u$ 12u$(small)'>
				<ul class='labeled-icons'>
					<li>
						<h3 class='icon fa-home'>
							<span class='label'>Address</span>
						</h3>
						1234 Somewhere Rd.<br />
						Nashville, TN 00000<br />
						United States
					</li>
					<li>
						<h3 class='icon fa-mobile'>
							<span class='label'>Phone</span>
						</h3>
						000-000-0000
					</li>
					<li>
						<h3 class='icon fa-envelope-o'>
							<span class='label'>Email</span>
						</h3>
						<a href='#'>hello@untitled.tld</a>
					</li>
				</ul>
			</div>
		</div>
	</section>
</div>
@stop

