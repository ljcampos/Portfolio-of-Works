<!DOCTYPE html>
<html>
<head>
	@include('includesHTML.head')
</head>
<body id='top'>
	<header>
		@include('includesHTML.header')
	</header>

	<section class='section'>
		@yield('content')
	</section>
	
	<footer>
		@include('includesHTML.footer')
	</footer>
</body>
</html>