<title>Portafolio de Trabajos</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="description" content="" />
<meta name="keywords" content="" />
{{HTML::script('js/jquery.min.js')}}
{{HTML::script('js/jquery.poptrox.min.js')}}
{{HTML::script('js/skel.min.js')}}
{{HTML::script('js/init.js')}}
<noscript>
	<link rel='stylesheet' href='css/skel.css' />
	<link rel='stylesheet' href='css/style.css' />
	<link rel='stylesheet' href='css/style-xlarge.css' />
</noscript>