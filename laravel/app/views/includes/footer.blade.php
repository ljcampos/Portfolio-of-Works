<div class='navbar footer navbar-fixed-bottom' role="navigation">
	<div class='container'>
		<p class='navbar-text pull-left'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. </p>
		<a class='navbar-btn btn-danger btn pull-right'>Suscribete</a>
	</div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
{{HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js')}}
<!-- Include all compiled plugins (below), or include individual files as needed -->
{{HTML::script('js/bootstrap.min.js')}}

<script>
    $('.carousel').carousel({
        interval: 3000
    })
</script>