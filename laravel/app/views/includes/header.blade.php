<!-- Header section -->
<nav class='navbar navbar-fixed-top navbar-inverse'>
	<div class='container-fluid'>
		<div class="navbar-header">
			<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#myNavbar'>
				<span class='icon-bar'></span>
				<span class='icon-bar'></span>
				<span class='icon-bar'></span>
			</button>
			<a class='navbar-brand' href='#'>Web Jobs</a>
		</div>

		<div class='nav navbar-nav navbar-right collapse navbar-collapse' id='myNavbar'>
			<ul class="nav navbar-nav">
				<li><a href='{{url('/')}}'>Home</a></li>
				<li><a href='{{url('/portfolio')}}'>Portafolio</a></li>
				<li><a href='{{url('/aboutme')}}'>About me</a></li>
			</ul>
		</div>
	</div>
</nav>
<!-- End Header section-->